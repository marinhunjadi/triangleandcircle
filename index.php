<?php
class Triangle
{
    private $a;
    private $b;
    private $c;
	private $h;
	private $perimeter;
	private $area;
	
	function __construct(float $a, float $b, float $c, float $h){
		$this->a = $a;
		$this->b = $b;
		$this->c = $c;
		$this->h = $h;
	}
	
	function getPerimeter(){
		return $this->a + $this->b + $this->c;	
	}
	
	function getArea(){
		return 0.5 * $this->a * $this->h;	
	}	

}

$a = 203;
$b = 145;
$c = 208;
$h = 137;
$triangle = new Triangle($a, $b, $c, $h);
echo "<p><h1>Trokut</h1></p>";
echo "<p>Opseg: " . $triangle->getPerimeter() . "</p>";
echo "<p>Površina: " . $triangle->getArea() . "</p>";

class Circle
{
    private $radius;
	private $circumference;
	private $area;
	
	function __construct(float $radius){
		$this->radius = $radius;
	}
	
	function getCircumference(){
		return 2 * pi() * $this->radius;	
	}
	
	function getArea(){
		return pi() * pow($this->radius, 2);	
	}	
}

$r = 10;
$circle = new Circle($r);
echo "<p><h1>Krug</h1></p>";
echo "<p>Opseg: " . $circle->getCircumference() . "</p>";
echo "<p>Površina: " . $circle->getArea() . "</p>";
?>